// import main SASS file
import '../../styles/scss/style.scss'

// Import Foundation
import 'foundation-sites'

// import JS modules to compile
import './init-foundation'
import './wp-foundation'
import './modules/fancybox'
import './modules/store-locator'
import './modules/navMenu'
import './modules/submenu'
import './modules/sliders'
import './modules/modules'
import './modules/gallery'
import './modules/wineDetail'
import './modules/search'
import './modules/animations'
import './modules/contact'
import './modules/ie'
// import './modules/select'
import './modules/accordion'
import './modules/scrollSlider'