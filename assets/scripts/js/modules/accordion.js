
$('.accordion-list-item').each((index, obj) => {
    $(obj).children('.answer').hide()
    $(obj).children('.question').on('click', (e) => {
        e.preventDefault()
        $(obj).children('.answer').fadeToggle("slow", "linear" )
        $(obj).children('.question').children('.toggle-identifier').html() == "+" ? $(obj).children('.question').children('.toggle-identifier').html('-') : $(obj).children('.question').children('.toggle-identifier').html('+');
    })
})