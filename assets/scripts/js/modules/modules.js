$(document).ready(() => {
    // Content & Detail
    $('.detail p').css('margin-bottom', '0')
    $('.detail em').parent('p').first().css('margin-top', '1rem')
    $('.detail em').parent('p').css('margin-bottom', '1rem')
    $('.detail strong').parent('p').css('margin-top', '2rem')


    $('.sbi-owl-nav .sbi-owl-prev').each((index, obj) => {
        $(obj).html('<img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-right.png" alt="arrow image">')
    })
    $('.sbi-owl-nav .sbi-owl-next').each((index, obj) => {
        $(obj).html('<img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-left.png" alt="arrow image">')
    })


    // Content Split
    var contentSplitCount,
        contentSplitElements = []

    new Promise((resolve, reject) => {
        $('.content-split').each((index, obj) => {
            contentSplitCount += index
            contentSplitElements.push(obj)
        })
        resolve()
    })
    .then(() => {
        if (contentSplitElements.length === 1) {
            $(contentSplitElements[0]).addClass('only-module-padding')
        } else {
            var first = contentSplitElements[0],
                last = contentSplitElements.slice(-1)[0]
            $(first).addClass('first-module-padding')
            $(last).addClass('last-module-padding')
        }
    })
    .catch((error) => {
        console.log(error)
    })
})






