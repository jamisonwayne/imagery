import swiper from 'swiper/dist/js/swiper'
import 'swiper/dist/css/swiper.min'



var feature_slider = new swiper('.feature-slider-container', {
    direction: 'horizontal',
    autoHeight: true,
    loop: false,
    effect: 'fade',
    autoplay: {
      delay: 5000,
    },
    

    // If we need pagination
    pagination: {
      el: '.feature-slider-controls',
      clickable: true,
    },

    on: {
      slideChangeTransitionEnd: () => {
          if ($('.swiper-slide-active .feature-video').length && $(window).width() < 1024) {
              $('.feature-image-container').toggleClass('video-slider-height');
          } else {
            $('.feature-image-container').removeClass('video-slider-height');
          }
      },
      init: () => {
        if ($('.swiper-slide-active .feature-video').length && $(window).width() < 1024) {
            $('.feature-image-container').addClass('video-slider-height');
        } else {
          $('.feature-image-container').removeClass('video-slider-height');
        }
    }
    }
})
