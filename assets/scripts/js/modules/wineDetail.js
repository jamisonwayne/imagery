$('.club-wine').each((index, obj) => {
    var $index = $(obj).attr('data-wine-id');

    $(obj).on('click', (e) => {
        e.preventDefault()

        new Promise((resolve, reject) => {
                $('.club-detail').each((index, obj) => {
                    $(obj).css({
                        opacity: '0',
                    })
                    $(obj).addClass('hide')
                    $('.disclaimer p').addClass('hide')
                })
                $('.club-wine img').each((index, obj) => {
                    $(obj).removeClass('bottle-up')
                })
                resolve()
            })
            .then(() => {
                $(obj).children('img').addClass('bottle-up')
                $('#club-detail-' + $index).toggleClass('hide')
                scrollToDetail()
            })
            .then(() => {
                setTimeout(() => {
                    $('#club-detail-' + $index).css({
                        opacity: '1',
                    })
                }, 200)
            })
            .catch((error) => {
                console.log(error)
            })
    })

    $('.club-detail').each((index, obj) => {
        $(obj).children('.close').on('click', (e) => {
            e.preventDefault()
            $(obj).addClass('hide')
            $('.disclaimer p').removeClass('hide')
            $('#club-wine-' + $index).children('img').removeClass('bottle-up')
        })
    })
})


function scrollToDetail() {
    var el = document.getElementById('wine-club-bottle-details')
    el.scrollIntoView({behavior: "smooth"})
}