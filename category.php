<?php get_header(); ?>

<div class="grid-container news">
    <div class="grid-x grid-margin-x grid-padding-x">
        <div class="cell small-10 small-offset-1 medium-12 medium-offset-0">
            <div class="grid-x grid-margin-x">
                <div class="cell medium-6 intro">
                    <h1 class="heading-1">News</h1>
                    <p>We need to come up with a little bit of copy to go here.</p>
                </div>
                <div class="cell medium-6 categories">
                    <!-- Select Categories -->
                    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="cell small-8 small-offset-2 large-6 large-offset-3">
                        <select name="news_categories" id="custom-post-type-categories-dropdown-single" class="postform">
                            <option value="-1">Select Category</option>

                            <?php
                            $categories = get_categories(array(
                                'taxonomy' => 'news_categories',
                            ));

                            foreach ($categories as $category) {
                                $category_link = sprintf( 
                                    '<option class="level-0" value="%2$s">%1$s</option>',
                                    esc_html( $category->name ),
                                    esc_html( $category->slug )
                                );
                                echo '<p class="category-link">' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</p> ';
                            }
                            ?>
                        </select>

                        <script>
                            (function() {
                                /* <![CDATA[ */
                                var dropdown = document.getElementById( "custom-post-type-categories-dropdown-single" );
                                function onCatChange() {
                                    if ( dropdown.options[dropdown.selectedIndex].value ) {
                                        return dropdown.form.submit();
                                    }
                                }
                                dropdown.onchange = onCatChange;
                            })();
                                /* ]]&gt; */
                        </script>

                    </form>
                </div>
            </div>

            <div class="grid-x grid-margin-x news-posts">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="cell medium-6 large-4 news-post">
                    <a href="<?php the_permalink(); ?>">

                        <!-- Featured Image -->
                        <?php if ( has_post_thumbnail() ):
                            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                            if ( ! empty( $large_image_url[0] ) ): ?>
                                <div class="news-featured-image" style="background-image: url(<?php echo $large_image_url[0]; ?>);"></div>       
                            <?php endif; ?>
                        <?php endif; ?>

                        <!-- Title -->
                        <h2 class="heading-3"><?php the_title(); ?></h2>

                        <div class="post-categories">
                        <?php
                            $post_categories = get_the_terms( $post->ID, 'news_categories' );

                            foreach ($post_categories as $category) {
                                $category_link = sprintf( 
                                    '<a href="%1$s" alt="%2$s">%3$s</a>',
                                    esc_url( get_category_link( $category->term_id ) ),
                                    esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
                                    esc_html( $category->name )
                                );
                                echo '<p class="category-link">' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</p> ';
                            }
                            ?> 
                        </div>
                    </a>
                </div>
            <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>