<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>


<footer class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x footer-wrapper">
        <div class="cell large-4 footer-left">
            <div class="social padding-xsmall">
                <ul>
                    <li><a target="_blank" href="https://www.instagram.com/imagerywinery/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.png" alt="Instagram"></a></li>
                    <li><a target="_blank" href="https://www.facebook.com/ImageryWinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.png" alt="Facebook"></a></li>
                    <li><a target="_blank" href="https://www.pinterest.com/imagerywinery/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pintrest.png" alt="Pintrest"></a></li>
                    <li><a target="_blank" href="https://twitter.com/imagerywinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.png" alt="Twitter"></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/user/BenzigerFamilyWinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.png" alt="Youtube"></a></li>
                    <li class="heading-2 uppercase">Connect</li>
                </ul>
            </div>
            <div class="press-links padding">
                <ul class="heading-4">
					<li><a href="<?php echo bloginfo('url'); ?>/news">News »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/contact">Contact Us »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/trade">Trade Resources »</a></li>
                </ul>
            </div>
            <div class="privacy-links">
                <ul class="heading-5">
                    <li><a href="<?php echo bloginfo('url'); ?>/terms-of-service">Terms of Service »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/privacy-policy">Privacy Policy »</a></li>
                    <li><a target="_blank" href="https://thewinegroup.com/supply-chain-transparency/">Supply Chain Transparency »</a></li>
                </ul>
            </div>
        </div>
        <div class="cell large-4 footer-middle">
            <div class="vertical-divider"></div>
            <a href="<?php echo bloginfo('url'); ?>/newsletter"><button class="btn-green">Subscribe to Our Mailing List</button></a>
        </div>
        <div class="cell large-4 footer-right">
            <a href="https://goo.gl/maps/rjdwTT7PEQT2" target="_blank">
                <div class="directions">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/directions.png" alt="Map Directions Image">
                    <p class="heading-2 uppercase">Get Directions</p>
                </div>
            </a>
            <div class="address">
                <ul class="heading-4">
                    <li>14335 Hwy 12</li>
                    <li>Glen Ellen, CA 95442</li>
                    <li>877-550-4278</li>
                </ul>
            </div>
            <div class="logo">
                <a target="_blank" href="https://www.benziger.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/benziger.png" alt="Benziger Logo"></a>
            </div>
        </div>
    </div>

    <div class="grid-x grid-margin-x footer-mobile-wrapper">
        <div class="cell small-7 footer-left">
            <div class="social">
                <p class="heading-2 uppercase">Connect</p>
                <ul>
                    <li><a target="_blank" href="https://www.instagram.com/imagerywinery/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.png" alt="Instagram"></a></li>
                    <li><a target="_blank" href="https://www.facebook.com/ImageryWinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.png" alt="Facebook"></a></li>
                    <li><a target="_blank" href="https://www.pinterest.com/imagerywinery/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pintrest.png" alt="Pintrest"></a></li>
                    <li><a target="_blank" href="https://twitter.com/imagerywinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.png" alt="Twitter"></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/user/BenzigerFamilyWinery"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.png" alt="Youtube"></a></li>
                </ul>
            </div>
            <div class="press-links padding-small">
                <ul class="heading-4">
                    <li><a href="<?php echo bloginfo('url'); ?>/news">News »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/contact">Contact Us »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/trade">Trade Resources »</a></li>
                </ul>
            </div>
        </div>

        <div class="cell small-5 footer-right">
            <a href="https://goo.gl/maps/rjdwTT7PEQT2" target="_blank">
                <div class="directions">
                    <p class="heading-2 uppercase">Get Directions</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/directions.png" alt="Map Directions Image">
                </div>
            </a>
            <div class="address padding-small">
                <ul class="heading-4">
                    <li>14335 Hwy 12</li>
                    <li>Glen Ellen, CA 95442</li>
                    <li>877-550-4278</li>
                </ul>
            </div>
        </div>

        <div class="cell footer-bottom">
            <a href="<?php echo bloginfo('url'); ?>/newsletter"><button class="btn-green">Subscribe to Our Mailing List</button></a>

            <div class="logo padding-small">
                <a target="_blank" href="https://www.benziger.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/benziger.png" alt="Benziger Logo"></a>
            </div>

            <div class="privacy-links">
                <ul class="heading-5">
                    <li><a href="<?php echo bloginfo('url'); ?>/terms-of-service">Terms of Service »</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/privacy-policy">Privacy Policy »</a></li>
                    <li><a target="_blank" href="https://thewinegroup.com/supply-chain-transparency/">Supply Chain Transparency »</a></li>
                </ul>
            </div>
		</div>	
	</div>

	<div class="grid-x">
		<div class="cell copyright">
			<p>&copy; <?php echo date('Y'); ?> Imagery Estate Winery. All Rights Reserved.</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>


<!-- SET AGE GATE PROPERTIES -->
<?php 
$age_gate = (object)array(
	'brand'						=> 		  'Set this is the brand name',
	'background_image'			=> 		  get_template_directory_uri().'/assets/images/age-gate.jpg',
	'brand_logo'				=>		  get_template_directory_uri().'/path-to-brand-logo-image',
	'age_heading'				=>		  'Discover Wines with Imagination.',
	'subheading'                =>        'But first, are you of legal drinking age?',
	'terms_of_service'     		=>		  'terms-of-service.html',
	'privacy_policy'			=>		  'privacy-policy.html',
	'regret_text'				=>		  'You must be 21 years of age or older to view this site.',

);
?>

<script>

			(function(factory){var jQuery;if(typeof define==='function'&&define.amd){define(['jquery'],factory);}else if(typeof exports==='object'){try{jQuery=require('jquery');}catch(e){}
			module.exports=factory(jQuery);}else{var _OldCookies=window.Cookies;var api=window.Cookies=factory(window.jQuery);api.noConflict=function(){window.Cookies=_OldCookies;return api;};}}(function($){var pluses=/\+/g;function encode(s){return api.raw?s:encodeURIComponent(s);}
			function decode(s){return api.raw?s:decodeURIComponent(s);}
			function stringifyCookieValue(value){return encode(api.json?JSON.stringify(value):String(value));}
			function parseCookieValue(s){if(s.indexOf('"')===0){s=s.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,'\\');}
			try{s=decodeURIComponent(s.replace(pluses,' '));return api.json?JSON.parse(s):s;}catch(e){}}
			function read(s,converter){var value=api.raw?s:parseCookieValue(s);return isFunction(converter)?converter(value):value;}
			function extend(){var key,options;var i=0;var result={};for(;i<arguments.length;i++){options=arguments[i];for(key in options){result[key]=options[key];}}
			return result;}
			function isFunction(obj){return Object.prototype.toString.call(obj)==='[object Function]';}
			var api=function(key,value,options){if(arguments.length>1&&!isFunction(value)){options=extend(api.defaults,options);if(typeof options.expires==='number'){var days=options.expires,t=options.expires=new Date();t.setMilliseconds(t.getMilliseconds()+days*864e+5);}
			return(document.cookie=[encode(key),'=',stringifyCookieValue(value),options.expires?'; expires='+options.expires.toUTCString():'',options.path?'; path='+options.path:'',options.domain?'; domain='+options.domain:'',options.secure?'; secure':''].join(''));}
			var result=key?undefined:{},cookies=document.cookie?document.cookie.split('; '):[],i=0,l=cookies.length;for(;i<l;i++){var parts=cookies[i].split('='),name=decode(parts.shift()),cookie=parts.join('=');if(key===name){result=read(cookie,value);break;}
			if(!key&&(cookie=read(cookie))!==undefined){result[name]=cookie;}}
			return result;};api.get=api.set=api;api.defaults={};api.remove=function(key,options){api(key,'',extend(options,{expires:-1}));return!api(key);};if($){$.cookie=api;$.removeCookie=api.remove;}
			return api;}));

			jQuery(function($) {
				$(document).ready(function() {
					/*!
					* Simple Age Verification (https://github.com/Herudea/age-verification))
					*/
					var modal_content, modal_screen;
					$(document).ready(function() {
						av_legality_check();
					});
					av_legality_check = function() {
						if ($.cookie('is_legal') == "yes") {} else {
							av_showmodal();
							$(window).on('resize', av_positionPrompt);
						}
					};
					av_showmodal = function() {
						modal_screen = $('<div id="modal_screen" class="age_gate"></div>');
						modal_content = $('<div id="modal_content" style="display:none"></div>');
						var modal_content_wrapper = $('<div id="modal_content_wrapper" class="content_wrapper"></div>');
						var modal_regret_wrapper = $('<div id="modal_regret_wrapper" class="content_wrapper" style="display:none;"></div>');	
						var content_agegate = $("<div class=\"grid-container\"><div class=\"grid-x grid-padding-x\"><div class=\"cell small-12 large-6 background\" style=\"background-image: url(<?php echo $age_gate->background_image ?>)\"></div><div class=\"cell medium-8 medium-offset-2 large-6 large-offset-0 copy-content\"><h1 class=\"text-center padding-small heading-1\"><?php echo $age_gate->age_heading; ?></h1><p><?php echo $age_gate->subheading; ?></p><nav class=\"\"><ul><li><a href=\"#\" class=\"av_btn\" rel=\"yes\" id=\"yes\"><button class=\"btn-black\">Yes</button></a></li><li><a href=\"#\" class=\"av_btn\" rel=\"no\" id=\"no\"><button class=\"btn-black\">No</button></a></li></nav><div class=\"agree\"><input type=\"checkbox\" id=\"agree\" class=\"btn-default\" required></input></a>I agree to the <a data-fancybox data-type=\"iframe\" data-src=\"<?php echo $age_gate->terms_of_service ?>\" href=\"javascript:;\">Terms of Service</a> and <a data-fancybox data-type=\"iframe\" data-src=\"<?php echo $age_gate->privacy_policy ?>\" href=\"javascript:;\">Privacy Policy</a></div><p class=\"small\">You must be at least 21 years old to view this site. By clicking “yes” you affirm that you are at least 21 years old.</p><p class=\"copyright small\">&copy; 2019 Imagery Estate Wintery. All Rights Reserved.</p></div></div></div>");
						var regret_text = $('<h2><?php echo $age_gate->regret_text ?></h2>');
						modal_content_wrapper.append(content_agegate);
						modal_regret_wrapper.append(regret_text);
						modal_content.append(modal_content_wrapper, modal_regret_wrapper);
						$('body').append(modal_screen, modal_content);
						av_positionPrompt();
						modal_content.find('a.av_btn').on('click', av_setCookie);
					};
					av_setCookie = function(e) {
						e.preventDefault();
						var is_legal = $(e.currentTarget).attr('rel');
						$.cookie('is_legal', is_legal, {
							expires: 30,
							path: '/'
						});
						if($("#agree").is(':checked') && (is_legal == "yes" )) {
							av_closeModal();
							$(window).off('resize'); 
						} else if (is_legal == "no") {
							av_showRegret();
						} else if (!$("#agree").is(':checked')) {
							alert('You must agree to the terms of service.');
						} else if (is_legal == "no") {
							av_showRegret();
						}
					};
					av_closeModal = function() {
						modal_content.fadeOut();
						modal_screen.fadeOut();
					};
					av_showRegret = function() {
						modal_screen.addClass('nope');
						modal_content.find('#modal_content_wrapper').hide();
						modal_content.find('#modal_regret_wrapper').show();
					};
					av_positionPrompt = function() {
						var top = ($(window).outerHeight() - $('#modal_content').outerHeight()) / 2;
						var left = ($(window).outerWidth() - $('#modal_content').outerWidth()) / 2;
						modal_content.css({
							'top': top,
							'left': left
						});
						if (modal_content.is(':hidden') && ($.cookie('is_legal') != "yes")) {
							modal_content.fadeIn('slow');
						}
					};
				});
			});
			jQuery(function($) {
				$(document).ready(function() {
					$('#modal_regret_wrapper').on('click', function() {
						$(this).hide();
						$('#modal_content_wrapper').show();
					});
				});
			});

		</script>
</body>

</html> <!-- end page --> 