<?php

// Adding WP Functions & Theme Support
function joints_theme_support()
{

	// Add WP Thumbnail Support
	add_theme_support('post-thumbnails');

	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);

	// Add RSS Support
	add_theme_support('automatic-feed-links');

	// Add Support for WP Controlled Title Tag
	add_theme_support('title-tag');

	// Add HTML5 Support
	add_theme_support(
		'html5',
		array(
			'comment-list',
			'comment-form',
			'search-form',
		)
	);

	add_theme_support('custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array('site-title', 'site-description'),
	));

	// Adding post format support
	add_theme_support(
		'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);


	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters('joints_theme_support', 1200);
} /* end theme support */

add_action('after_setup_theme', 'joints_theme_support');



register_taxonomy(
	'wine_categories', # Taxonomy name
	array('wines'), # Post Types
	array( # Arguments
		'labels'            => array(
			'name'              => 'Wine Categories',
			'singular_name'     => 'Wine Category',
			'search_items'      => 'Search Wine Categories',
			'all_items'         => 'All Wine Categories',
			'parent_item'       => 'Parent Wine Category',
			'parent_item_colon' => 'Parent Wine Category',
			'view_item'         => 'View Wine Category',
			'edit_item'         => 'Edit Wine Category',
			'update_item'       => 'Update Wine Category',
			'add_new_item'      => 'Add New Wine Category',
			'new_item_name'     => 'New Wine Category Name',
			'menu_name'         => 'Wine Categories',
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'wine-categories' ),
	)
);


// REGISTER EVENTS SIDEBAR
if ( function_exists('register_sidebar') )
	if ( function_exists('register_sidebar') )
		register_sidebar(array(
		'name'=>'Events Sidebar',
		//'id'=>'events-sidebar',
		'description' => ''
));


register_taxonomy(
	'news_categories', # Taxonomy name
	array('news'), # Post Types
	array( # Arguments
		'labels'            => array(
			'name'              => 'News Categories',
			'singular_name'     => 'News Category',
			'search_items'      => 'Search News Categories',
			'all_items'         => 'All News Categories',
			'parent_item'       => 'Parent News Category',
			'parent_item_colon' => 'Parent News Category',
			'view_item'         => 'View News Category',
			'edit_item'         => 'Edit News Category',
			'update_item'       => 'Update News Category',
			'add_new_item'      => 'Add New News Category',
			'new_item_name'     => 'New News Category Name',
			'menu_name'         => 'News Categories',
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'news-categories' ),
	)
);

function modify_product_cat_query( $query ) {
	if (is_page('news') || is_tax('news-categories')) {
		$query->set('posts_per_page', 9);
	}
  }
  add_action( 'pre_get_posts', 'modify_product_cat_query' );