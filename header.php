<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <meta class="foundation-mq">

    <!-- If Site Icon isn't set in customizer -->
    <!--
		<?php if (!function_exists('has_site_icon') || !has_site_icon()) { ?>
	
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />	
	    <?php 
	} ?>
-->

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <script src="https://unpkg.com/es6-promise/dist/es6-promise.auto.min.js"></script>
    <script src="https://unpkg.com/unfetch/polyfill/index.js"></script>

    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NXDJDVF');</script>
	<!-- End Google Tag Manager -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92375808-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-92375808-1');
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1112927128841390');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" alt="Facebook" style="display:none"
  src="https://www.facebook.com/tr?id=1112927128841390&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'clickserv.basis.net/conv/d5f6b69f6c22efdb';new Image().src = ssaUrl; (function(d) {  var syncUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel-a.basis.net/dmp/asyncPixelSync'; var iframe = d.createElement('iframe'); (iframe.frameElement || iframe).style.cssText = "width: 0; height: 0; border: 0;"; iframe.src = "javascript:false"; d.body.appendChild(iframe); var doc = iframe.contentWindow.document; doc.open().write('<body onload="window.location.href=\''+syncUrl+'\'">'); doc.close(); })(document); </script>

<!-- Activity name for this tag: Imagery Winery Unique Daily Visits -->
<!-- URL of the webpage where the tag will be placed: https://imagerywinery.com/ -->
<!-- <script type='text/javascript'>
var axel = Math.random()+"";
var a = axel * 10000000000000;
document.write('<img src="https://pubads.g.doubleclick.net/activity;xsp=4367494;ord=1;num='+ a +'?" width=1 height=1 border=0/>');
</script>
<noscript>
<img alt="Doubleclick" src="https://pubads.g.doubleclick.net/activity;xsp=4367494;ord=1;num=1?" width=1 height=1 border=0/>
</noscript> -->


<script type='text/javascript'>
window.__lo_site_id = 106211;

(function() {
var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
})();
</script>

<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel-a.basis.net/iap/3fe0d63c26819ef5';new Image().src = ssaUrl; (function(d) { var syncUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel-a.basis.net/dmp/asyncPixelSync'; var iframe = d.createElement('iframe'); (iframe.frameElement || iframe).style.cssText = "display: none; width: 0; height: 0; border: 0;"; iframe.src = "javascript:false"; d.body.appendChild(iframe); var doc = iframe.contentWindow.document; doc.open().write('<body onload="window.location.href=\''+syncUrl+'\'">'); doc.close(); })(document); </script>

<script async src='https://tag.simpli.fi/sifitag/29531b60-6dbd-0136-4c5e-067f653fa718'></script>

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://use.typekit.net/lnq7upk.css">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


    <header class="header" role="banner">
        <!-- This navs will be applied to the topbar, above all content 
		To see additional nav styles, visit the /parts directory -->
        <div class="grid-x">
            <?php get_template_part('parts/nav', 'title-bar'); ?>
        </div>
    </header> <!-- end .header --> 

    <!-- Search Box -->
    <div class="search-box">
        <img id="search-close" src="<?php echo get_template_directory_uri(); ?>/assets/images/close.svg" alt="Close Icon">
        <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
    </div>