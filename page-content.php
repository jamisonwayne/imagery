<?php 
    /** Template Name: Content */ 

    get_header();
?>

<div class="grid-container full content-template-container grey">
    <div class="grid-x content-template">
        <div class="cell small-10 small-offset-1 medium-8 medium-offset-2 large-6 large-offset-3">
            <?php
                if (have_posts()) : 
                    while (have_posts()) : the_post(); 
                        the_content();
                    endwhile; 
                endif;
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>