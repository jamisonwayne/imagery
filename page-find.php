<?php 
    /** Template Name: Find */ 

    get_header();
?>

<div class="grid-container find">
    <div class="grid-x">
        <div class="cell medium-8 medium-offset-2 large-6 large-offset-3 title">
            <h1 class="heading-1">Find A Retailer</h1>
            <p>Find Imagery Wines near You</p>
        </div>
        <div class="cell small-10 small-offset-1 medium-12  medium-offset-0 where-to-buy">
            <?php echo do_shortcode('[SLPLUS]'); ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>