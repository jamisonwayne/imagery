<?php 
    /** Template Name: Form Template */ 

    get_header();
?>

<div class="grid-container form">
    <div class="grid-x grid-margin-x">
        <div class="cell title"><h1 class="heading-1"><?php the_field('main_heading'); ?></h1></div>
        <div class="cell medium-7 image-container with-divider">
            <div class="image" style="background-image: url(<?php the_field('image'); ?>);"></div>
        </div>
        <div class="cell medium-5 form-wrapper">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>

            <?php endwhile; endif; ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>