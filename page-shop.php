<?php 
    /** Template Name: Shop Landing */ 

    get_header();
?>

<div class="grid-container full shop-landing green">
    <?php if (!get_field('hide_featured_sale')) : ?>
        <div class="grid-x featured-sale-block">
            <div class="cell small-10 small-offset-1">
            <div class="grid-x">
                <div class="cell medium-7 medium-5 large-6 content">
                        <h1 class="heading-1"> <?php the_field('featured_sale_heading'); ?></h1>
                        <?php the_field('featured_sale_description'); ?>
                </div>
                    <div class="cell medium-7 medium-5 large-6 cta">
                        <!-- Check CTA/URL Type -->
                        <?php if (!empty(get_field('featured_sale_cta') && empty(get_field('featured_sale_external_link')))) : ?>
                            <a target="_blank" href="<?php the_field('featured_sale_cta_link'); ?>"><button class="btn-black"><?php the_field('featured_sale_cta'); ?></button></a>
                        <?php elseif (!empty(get_field('featured_sale_cta') && !empty(get_field('featured_sale_external_link')))) :  ?>
                            <a target="_blank" href="<?php the_field('featured_sale_external_link'); ?>"><button class="btn-black"><?php the_field('featured_sale_cta'); ?></button></a>
                        <?php endif; ?>
                    </div>
            </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="grid-container shop-landing">
    <div class="grid-x grid-margin-x grid-padding-x callout-blocks">
        <div class="cell small-12 medium-8 medium-offset-2 large-6 large-offset-3 main-content">
            <span class="heading-1"><?php the_field('main_heading'); ?></span>
            <?php the_field('description'); ?>

             <!-- Check CTA/URL Type -->
             <?php if (!empty(get_field('main_cta') && empty(get_field('main_external_link')))) : ?>
                <a target="_blank" href="<?php the_field('main_cta_link'); ?>"><button class="btn-black"><?php the_field('main_cta'); ?></button></a>
            <?php elseif (!empty(get_field('main_cta') && !empty(get_field('main_external_link')))) :  ?>
                <a target="_blank" href="<?php the_field('main_external_link'); ?>"><button class="btn-black"><?php the_field('main_cta'); ?></button></a>
            <?php endif; ?>
        </div>
        <?php if ( have_rows('blocks') ) : ?>
            <?php while( have_rows('blocks') ) : the_row(); ?>
        
                <div class="cell medium-6 callout-block" style="background-image: url(<?php the_sub_field('image'); ?>);">
                    <!-- Check CTA/URL Type -->
                    <?php if (!empty(get_sub_field('cta') && empty(get_sub_field('external_link')))) : ?>
                        <a target="_blank" href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                    <?php elseif (!empty(get_sub_field('cta') && !empty(get_sub_field('external_link')))) :  ?>
                        <a target="_blank" href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                    <?php endif; ?>
                </div>
        
            <?php endwhile; ?>
        <?php endif; ?>
        
    </div>
</div>



<?php get_footer(); ?>