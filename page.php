<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all the pages by default.
 */

get_header(); ?>

        <?php get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; endif; ?>
        

<?php get_footer(); ?> 