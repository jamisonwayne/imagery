<div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="large" itemscope itemtype="http://schema.org/Organization">
    <a class="logo" href="<?php echo bloginfo('url'); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" itemprop="logo">
    </a>
    <div class="imagery-toggle" data-toggle="example-animated-menu">
        <img src="<?php echo get_template_directory_uri() . '/assets/images/hamburger.svg'; ?>"/>
    </div>
</div>

<div class="top-bar-container" id="example-animated-menu" data-toggler data-animate="fade-in fade-out" data-closable itemscope itemtype="http://schema.org/Organization">
    <!-- Close Icon -->
    <div class="menu-close" data-close data-hide-for="large">
    <img src="<?php echo get_template_directory_uri() . '/assets/images/close.svg'; ?>"/>
    </div>
    <div class="top-bar">
        <div class="menu-container">

            <!--  Logo -->
            <a class="imagery-logo-wrapper" href="<?php echo bloginfo('url'); ?>">
                <img class="imagery-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" itemprop="logo">
            </a>

            <?php 
			wp_nav_menu(array(
				'menu' => 'Menu-left',
				'container_class' => 'top-bar-left',
			));
			?>

            <div class="mobile divider-line"></div>

            <div class="menu-right">
                <ul>
                    <li>
                        <a href="<?php echo bloginfo('url'); ?>/find">Find</a>
                    </li>
                    <!-- <li><?php echo do_shortcode('[dcomm-cart-presence type="list"]'); ?></li> -->
                    <li><a href="https://shopimagerywinery.vintegrate.com/login">Sign In</a></li>
                    <li><a href="https://shopimagerywinery.vintegrate.com/cart">Cart</a></li>
                    <li id="search"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/search.svg" alt="Search Icon"></li>
                </ul>
            </div>

        </div>
    </div>
</div> 
