<?php get_header(); ?>

<div class="grid-container news-single">
    <div class="grid-x grid-margin-x grid-padding-x">
        <div class="cell categories">
            <!-- Select Categories -->
            <form action="<?php echo bloginfo('url'); ?>" method="get" class="cell small-8 small-offset-2 large-4 large-offset-4">
                <select name="news_categories" id="custom-post-type-categories-dropdown-single" class="postform">
                    <option value="-1">Select Category</option>

                    <?php
                    $categories = get_categories(array(
                        'taxonomy' => 'news_categories',
                    ));

                    foreach ($categories as $category) {
                        $category_link = sprintf( 
                            '<option class="level-0" value="%2$s">%1$s</option>',
                            esc_html( $category->name ),
                            esc_html( $category->slug )
                        );
                        echo '<p class="category-link">' . sprintf( esc_html__( '%s', bloginfo('url') ), $category_link ) . '</p> ';
                    }
                    ?>
                </select>

                <script>
                    (function() {
                        /* <![CDATA[ */
                        var dropdown = document.getElementById( "custom-post-type-categories-dropdown-single" );
                        function onCatChange() {
                            if ( dropdown.options[dropdown.selectedIndex].value ) {
                                return dropdown.form.submit();
                            }
                        }
                        dropdown.onchange = onCatChange;
                    })();
                        /* ]]&gt; */
                </script>

            </form>
        </div>
    </div>

    <div class="grid-x grid-margin-x grid-padding-x">
        <div class="cell small-10 small-offset-1 medium-8 medium-offset-2 large-8 large-offset-2 news-post">
            
                <h1 class="heading-1"><?php the_title(); ?></h1>

                <!-- Featured Image -->
                <?php if ( has_post_thumbnail() ):
                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                    if ( ! empty( $large_image_url[0] ) ): ?>
                        <div class="news-featured-image" style="background-image: url(<?php echo $large_image_url[0]; ?>);"></div>       
                    <?php endif; ?>
                <?php endif; ?>

                <p><?php echo $post->post_content; ?></p>
        </div>
    </div>

    <!-- Pagination -->
    <div class="grid-x">
        <div class="cell medium-8 medium-offset-2 large-8 large-offset-2 news-pagination">
        <?php
            $previous_post = get_previous_post()->ID;
            $next_post = get_next_post()->ID;
        ?>
            <?php if ($next_post) : ?>
                <a href="<?php echo esc_url( get_permalink( $next_post ) ); ?>"><button class="btn-underline">Previous</button></a>
            <?php endif; ?>
            <?php if ($previous_post) : ?>
                <a href="<?php echo esc_url( get_permalink( $previous_post ) ); ?>"><button class="btn-underline">Next</button></a>
            <?php endif; ?>
        </div>  
    </div>
</div>

<?php get_footer(); ?>