<div class="grid-container full content-image-callout" data-module-key="<?php echo $moduleIndex['content_&_image_callout'] ?>" id="content-image-callout-<?php echo $moduleIndex['content_&_image_callout']; ?>">
    <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <div class="grid-x grid-padding-x grid-margin-x content anim">
        <div class="cell small-12 large-6 large-offset-3">
            <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
            <?php the_sub_field('description'); ?>
        </div>
    </div>
    <div class="grid-x image-callout anim">
        <div class="cell medium-10 medium-offset-1 small-12 large-10 large-offset-1 xlarge-8 xlarge-offset-2">
            <div class="image" style="background-image: url(<?php the_sub_field('callout_image'); ?>);">
                <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <div class="image-description"><span><?php the_sub_field('image_description'); ?></span></div>
            </div>
        </div>
    </div>
</div>