<div class="grid-container content-links" data-module-key="<?php echo $moduleIndex['content_&_links'] ?>" id="content-links-<?php echo $moduleIndex['content_&_links']; ?>">
    <div class="grid-x grid-margin-x anim">
        <div class="cell medium-8 large-6 xlarge-5  content">
            <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
            <p><?php the_sub_field('description'); ?></p>
        </div>
        <div class="cell link-container">
            <div class="links">
                <?php if ( have_rows('links') ) : $i = 0; ?>
                    <?php while( have_rows('links') ) : the_row(); $i++; ?>
                        <div class="link">
                            <!-- Check CTA/URL Type -->
                            <?php if (empty(get_sub_field('external_link'))) : ?>
                                <a href="<?php the_sub_field('link'); ?>" <?php echo ($i == 1 ? 'class="active-link"' : ''); ?>><button class="btn-underline"><?php the_sub_field('link_title'); ?></button></a>
                            <?php elseif (!empty(get_sub_field('external_link'))) :  ?>
                                <a href="<?php the_sub_field('external_link'); ?>" <?php echo ($i == 1 ? 'class="active-link"' : ''); ?>><button class="btn-underline"><?php the_sub_field('link_title'); ?></button></a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>