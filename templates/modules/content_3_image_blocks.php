<div class="grid-container content-3-image-blocks" data-module-key="<?php echo $moduleIndex['content_3_image_blocks']; ?>" id="content-3-image-blocks-<?php echo $moduleIndex['content_3_image_blocks']; ?>">
    <!-- Top Shadow -->
    <?php if (get_sub_field('top_shadow') && !get_sub_field('shadow_offset')): ?>
        <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>
    <?php if (get_sub_field('top_shadow') && get_sub_field('shadow_offset')): ?>
        <img class="shadow-border shadow-offset" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>

    <div class="grid-x grid-margin-x anim">
        <?php if (!empty(get_sub_field('main_heading'))) : ?>
            <div class="cell main-content">
                <h2 class="heading-1"><?php the_sub_field('main_heading'); ?></h2>
                <?php the_sub_field('description'); ?>
                
                <?php if (empty(get_sub_field('external_link')) && !empty(get_sub_field('cta'))) : ?>
                    <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php elseif (!empty(get_sub_field('external_link')) && !empty(get_sub_field('cta'))) :  ?>
                    <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php endif; ?>
                
            </div>
        <?php endif; ?>
        
        <!-- Large Block -->
        <div class="cell large-8 <?php echo (get_sub_field('reverse')) ? 'small-order-2' : ''; ?>">
        <?php if (empty(get_sub_field('external_link')) && !empty(get_sub_field('cta')) && get_sub_field('link_images')) : ?>
            <a class="block-link" href="<?php the_sub_field('cta_link'); ?>">
        <?php endif; ?>
        <?php if (!empty(get_sub_field('external_link')) && empty(get_sub_field('cta')) && get_sub_field('link_images')) : ?>
            <a class="block-link" href="<?php the_sub_field('external_link'); ?>">
        <?php else: ?>
        <?php endif; ?>
            <div class="grid-x grid-margin-y grid-margin-x">
            <?php $large_block_type = get_sub_field('large_block_type'); ?>
                <div class="cell large-block <?php echo ($large_block_type == 'Image' ? 'image-content' : 'copy-content'); ?> <?php echo (get_sub_field('reverse')) ? 'small-order-2' : ''; ?> <?php echo (get_sub_field('align_text_top')) ? 'align-text-top' : ''; ?>" style="background-image: url(<?php if ($large_block_type == 'Image') echo the_sub_field('large_block_image'); ?>)">
                    <?php if (get_sub_field('large_block_type') == 'Content') : ?>
                        <div class="content">
                            <?php the_sub_field('large_block_content'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (get_sub_field('link_images')) : ?>
                </a>
            <?php endif; ?>  
        </div>

        <?php $block_margin = (is_page(array('Imagery California', 'Our Winemaker', 'Private Events'))) ? 'block-margin' : ''; ?>

        <!-- Small Blocks -->
        <div class="cell large-4 small-blocks <?php echo $block_margin; ?> <?php echo (get_sub_field('reverse')) ? 'small-order-1' : ''; ?>">
            <div class="grid-x">
                <div class="cell">
                <?php if (empty(get_sub_field('external_link')) && !empty(get_sub_field('cta')) && get_sub_field('link_images')) : ?>
                    <a class="block-link" href="<?php the_sub_field('cta_link'); ?>">
                <?php endif; ?>
                <?php if (!empty(get_sub_field('external_link')) && empty(get_sub_field('cta')) && get_sub_field('link_images')) : ?>
                    <a class="block-link" href="<?php the_sub_field('external_link'); ?>">
                <?php else: ?>
                <?php endif; ?>
                    <?php 
                        $default_margin_x = (!is_page(array('Imagery California', 'Our Winemaker', 'Art', 'Private Events'))) ? 'grid-margin-x' : null;
                    ?>
                    <div class="grid-x <?php echo $default_margin_x; ?> grid-margin-y">

                        <?php 
                        $art_copy = (is_page('Art')) ? 'small-12' : null;
                        $art_image = (is_page('Art')) ? 'small-8 small-offset-2 medium-6 medium-offset-3 large-12 large-offset-0' : null;
                        $private_events_copy = (is_page('Private Events')) ? 'small-12' : null;
                        $private_events_image = (is_page('Private Events')) ? 'small-8 small-offset-2 medium-6 medium-offset-3 large-12 large-offset-0' : null;
                        $imagery_california_copy = (is_page('Imagery California')) ? 'small-12' : null;
                        $imagery_california_image = (is_page('Imagery California')) ? 'small-8 small-offset-2 medium-6 medium-offset-3 large-12 large-offset-0' : null;
                        $our_winemaker_copy = (is_page('Our Winemaker') && !get_sub_field('reverse')) ? 'small-12' : null;
                        $our_winemaker_image = (is_page('Our Winemaker') && !get_sub_field('reverse')) ? 'small-8 small-offset-2 medium-6 medium-offset-3 large-12 large-offset-0' : null;
                        $our_winemaker_copy_reverse = (is_page('Our Winemaker') && get_sub_field('reverse')) ? 'small-12 small-order-2 large-order-1' : null;
                        $our_winemaker_image_reverse = (is_page('Our Winemaker') && get_sub_field('reverse')) ? 'small-8 small-offset-2 medium-6 medium-offset-3 large-12 large-offset-0 small-order-1 large-order-2' : null;
                        $default_small_block = (!is_page(array('Imagery California', 'Our Winemaker', 'Art', 'Private Events'))) ? 'small-6 medium-6 medium-offset-0 large-12' : null;
                        $small_block_top_type = get_sub_field('small_block_top_type');
                        ?>
                        <div class="cell <?php echo $private_events_image; echo $art_copy; echo $imagery_california_image; echo $default_small_block; echo $our_winemaker_image; echo $our_winemaker_copy_reverse; ?> small-block-top <?php echo ($small_block_top_type == 'Image' ? 'image-content ' : 'copy-content'); ?> <?php echo (get_sub_field('reverse')) ? 'text-right' : ''; ?> <?php echo (get_sub_field('align_text_top')) ? 'align-text-top' : ''; ?>" style="background-image: url(<?php if ($small_block_top_type == 'Image') echo the_sub_field('small_block_top_image'); ?>)">
                            <?php if (get_sub_field('small_block_top_type') == 'Content') : ?>
                                <div class="content"> 
                                    <?php the_sub_field('small_block_top_content'); ?>
                                </div>
                            <?php endif; ?>
                        </div>

                        <?php $small_block_bottom_type = get_sub_field('small_block_bottom_type'); ?>
                        <div class="cell <?php echo $private_events_copy; echo $art_image; echo $imagery_california_copy; echo $default_small_block; echo $our_winemaker_copy; echo $our_winemaker_image_reverse; ?> small-block-bottom <?php echo ($small_block_bottom_type == 'Image' ? 'image-content' : 'copy-content '); ?> <?php echo (get_sub_field('align_text_top')) ? 'align-text-top' : ''; ?>" style="background-image: url(<?php if ($small_block_bottom_type == 'Image') echo the_sub_field('small_block_bottom_image'); ?>)">
                            <?php if (get_sub_field('small_block_bottom_type') == 'Content') : ?>
                                <div class="content">
                                    <?php the_sub_field('small_block_bottom_content'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (get_sub_field('link_images')) : ?>
                        </a>
                    <?php endif; ?> 
                </div>
            </div>
            
        </div>
        <!-- paragraph block -->
        <?php if (get_sub_field('paragraph_block')): ?>
            <div class="cell large-10 large-offset-1 small-order-3 paragraph-block">
                <?php the_sub_field('paragraph_block'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>