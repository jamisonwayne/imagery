<div class="grid-container content-3-image-callout" data-module-key="<?php echo $moduleIndex['content_3_image_callout'] ?>" id="content-3-image-callout-<?php echo $moduleIndex['content_3_image_callout']; ?>">
    <!-- Top Shadow -->
    <?php if (get_sub_field('top_shadow') && !get_sub_field('shadow_offset')): ?>
        <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>
    <?php if (get_sub_field('top_shadow') && get_sub_field('shadow_offset')): ?>
        <img class="shadow-border shadow-offset" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>
    
    <div class="grid-x grid-margin-x grid-padding-x grid-margin-y anim">
        <div class="cell medium-10 medium-offset-1 large-6 large-offset-3 content">
            <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
            <?php the_sub_field('description'); ?>
        </div>
        <div class="cell medium-6 medium-offset-3 large-10 large-offset-1">
            <div class="grid-x grid-margin-x">
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_1'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_1'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_1'); ?></p>
                        <button class="btn-underline"><?php the_sub_field('cta_1'); ?></button>
                    </a>
                </div>
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_2'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_2'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_2'); ?></p>
                        <button class="btn-underline"><?php the_sub_field('cta_2'); ?></button>
                    </a>
                </div>
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_3'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_3'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_3'); ?></p>
                        <button class="btn-underline"><?php the_sub_field('cta_3'); ?></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>