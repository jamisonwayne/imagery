<?php 
    $backgroundClass;
    if (get_sub_field('background_color') == 'white'):
        $backgroundClass =  'white';
    elseif (get_sub_field('background_color') == 'grey'): 
        $backgroundClass = 'grey';
    elseif (get_sub_field('background_color') == 'greydark'):
        $backgroundClass = 'grey-dark';
    elseif (get_sub_field('background_color') == 'gold'):
        $backgroundClass = 'gold';
    elseif (get_sub_field('background_color') == 'green'):
        $backgroundClass = 'green';
    endif;
?>

<div class="grid-container full content-block <?php echo $backgroundClass; ?>" data-module-key="<?php echo $moduleIndex['content_block'] ?>" id="content-block-<?php echo $moduleIndex['content_block']; ?>">
    <?php if (get_sub_field('top_shadow') && !get_sub_field('shadow_offset')): ?>
        <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>
    <?php if (get_sub_field('top_shadow') && get_sub_field('shadow_offset')): ?>
        <img class="shadow-border shadow-offset" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>

    <!-- BASIC -->
    <?php if (get_sub_field('content_type') == 'basic') : ?>
    <div class="grid-x grid-margin-x grid-padding-x content basic anim">
        <?php if(get_sub_field('align_left')):  ?>
            <div class="cell small-10 small-offset-1 <?php echo (get_sub_field('compact')) ? 'large-6' : 'large-10 large-offset-1'; ?> text-align-left">
                <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
                <?php the_sub_field('description'); ?>
            </div>
        <?php else: ?>
            <div class="cell medium-10 medium-offset-1 <?php echo (get_sub_field('compact')) ? 'large-8 large-offset-2' : 'large-10 large-offset-1'; ?>">
                <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
                <?php the_sub_field('description'); ?>
            </div>
        <?php endif; ?>
    </div>

    <!-- LIST -->
    <?php elseif (get_sub_field('content_type') == 'list') : ?>
    <div class="grid-x grid-padding-x grid-margin-x grid-padding-y list anim">
        <div class="cell medium-10 medium-offset-1 xlarge-6 xlarge-offset-3">
            <div class="grid-x">
                <div class="cell title">
                    <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
                </div>
                <?php if ( have_rows('list') ) : ?>
                    <?php while( have_rows('list') ) : the_row(); ?>
                        <div class="cell list-item">
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="cell cta">
                     <?php if (empty(get_sub_field('external_link'))) : ?>
                        <a href="<?php the_sub_field('list_cta_link'); ?>"><button class="btn-black"><?php the_sub_field('list_cta'); ?></button></a>
                    <?php elseif (!empty(get_sub_field('external_link'))) :  ?>
                        <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('list_cta'); ?></button></a>
                    <?php endif; ?>
                </div> 
            </div>
        </div>
    </div>

    <!-- MULTIPLE LIST -->
    <?php elseif (get_sub_field('content_type') == 'multipleList') : ?>
    <div class="grid-x grid-padding-x grid-margin-x grid-padding-y multiple-list anim">
        <div class="cell medium-10 medium-offset-1 xlarge-6 xlarge-offset-3">
            <div class="grid-x">

                <?php if ( have_rows('multiple_list') ) : ?>
                    <?php while( have_rows('multiple_list') ) : the_row(); ?>
                    <div class="list">
                            <div class="cell title">
                                <span class="heading-1"><?php the_sub_field('title'); ?></span>
                            </div>

                            <?php if ( have_rows('list') ) : ?>
                                <?php while( have_rows('list') ) : the_row(); ?>
                                    <div class="cell list-item">
                                        <p><?php the_sub_field('description'); ?></p>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if (!empty(get_sub_field('cta'))): ?>
                                <div class="cell cta">
                                    <!-- Check CTA/URL Type -->
                                    <?php if (empty(get_sub_field('external_link'))) : ?>
                                        <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                                    <?php elseif (!empty(get_sub_field('external_link'))) :  ?>
                                        <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?> 
            </div>
        </div>
    </div>


    <!-- MULTIPLE LIST WITH LINKS -->
    <?php elseif (get_sub_field('content_type') == 'multipleListWithLinks') : ?>
    <div class="grid-x grid-padding-x grid-margin-x grid-padding-y multiple-list multiple-list-with-links">
        <div class="cell medium-10 medium-offset-1 xlarge-6 xlarge-offset-3">
            <div class="grid-x">

                <?php if ( have_rows('multiple_list_with_links') ) : ?>
                    <?php while( have_rows('multiple_list_with_links') ) : the_row(); ?>
                    <div class="list" id="<?php the_sub_field('list_id'); ?>">
                            <div class="cell title">
                                <span class="heading-1"><?php the_sub_field('title'); ?></span>
                            </div>

                            <?php if ( have_rows('file_list') ) : ?>
                                <?php while( have_rows('file_list') ) : the_row(); ?>
                                    <div class="cell file-list-container">
                                        <p><strong><?php the_sub_field('subtitle'); ?></strong></p>

                                        <?php if ( have_rows('files') ) : ?>
                                            <?php while( have_rows('files') ) : the_row(); ?>
                                        
                                            <!-- Check if list item is a file link or video URL -->
                                            <?php if (!get_sub_field('video')) : ?>
                                            <div class="list-item">
                                                <a traget="_blank" class="paragraph-1" href="<?php the_sub_field('file'); ?>"><?php the_sub_field('file_title'); ?></a>
                                            </div>
                                            <?php elseif (get_sub_field('video')): ?>
                                                <iframe 
                                                    width="560" 
                                                    height="315" 
                                                    src="<?php the_sub_field('video_url'); ?>" 
                                                    frameborder="0" 
                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                                                    allowfullscreen>
                                                </iframe>
                                            <?php endif; ?>
                                        
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                        
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?> 
            </div>
        </div>
    </div>


    <!-- Accordion List -->
    <?php elseif (get_sub_field('content_type') == 'accordionList') : ?>
    <div class="grid-x grid-padding-x grid-margin-x grid-margin-y grid-padding-y accordion-list anim">
        <div class="cell medium-10 medium-offset-1 xlarge-6 xlarge-offset-3 content">
            <div class="cell title">
                <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
                <?php the_sub_field('description'); ?>
            </div>

            <?php if ( have_rows('accordion') ) : ?>
                <?php while( have_rows('accordion') ) : the_row(); ?>
                    <div class="accordion-list-item">
                        <div class="question">
                            <div class="toggle-identifier">+</div>
                            <p><?php the_sub_field('title'); ?></p>
                        </div>
                        <p class="answer"><?php the_sub_field('description'); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>
</div>

