<?php 
    $backgroundClass;
    if (get_sub_field('background_color') == 'white'):
        $backgroundClass =  'white';
    elseif (get_sub_field('background_color') == 'grey'): 
        $backgroundClass = 'grey';
    elseif (get_sub_field('background_color') == 'greydark'):
        $backgroundClass = 'grey-dark';
    elseif (get_sub_field('background_color') == 'gold'):
        $backgroundClass = 'gold';
    endif;

    $id = get_sub_field('slider_id');
?>

<div class="grid-container full content-scroll-slider <?php echo $backgroundClass; ?>" data-content-scroll-id="<?php echo $id; ?>" data-module-key="<?php echo $moduleIndex['content_scroll_slider'] ?>" id="content-scroll-slider-<?php echo $moduleIndex['content_scroll_slider']; ?>">
    <?php if (get_sub_field('top_shadow') && !get_sub_field('shadow_offset')): ?>
        <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>
    <?php if (get_sub_field('top_shadow') && get_sub_field('shadow_offset')): ?>
        <img class="shadow-border shadow-offset" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <?php endif; ?>

    <div class="grid-x grid-padding-x content anim">
        <div class="cell small-12 large-6 large-offset-3">
            <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
            <?php the_sub_field('description'); ?>
        </div>
    </div>

    <!-- Scroll Slider -->
    <div id="scrollPinContainer-<?php echo $id; ?>" class="grid-x scrollPinContainer scrollPinContainer-<?php echo $id; ?> anim">
        <div id="scrollSlider-<?php echo $id; ?>" class="cell small-10 small-offset-1 large-8 large-offset-2 scrollSlider scrollSlider-<?php echo $id; ?>">
            <div class="swiper-wrapper">
                <?php $slider_shadow = get_sub_field('slider_shadow'); ?>
                <!-- Slides -->
                <?php if ( have_rows('slides') ) : ?>
                    <?php while( have_rows('slides') ) : the_row(); ?>
                        <div class="swiper-slide scrollSlide scrollSlide-<?php echo $id; ?>">
                            <div class="image" style="background-image: url(<?php the_sub_field('slide_image'); ?>);">
                                <!-- Slider Shadow -->
                                <?php if ($slider_shadow) : ?>
                                    <img class="shadow-border-bottom" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
                                <?php endif; ?>
                                
                            </div>
                            <?php if (get_sub_field('author')) : ?>
                                <div class="author">
                                    <p><?php the_sub_field('author'); ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <!-- Check CTA/URL Type -->
            <?php if (!empty(get_sub_field('cta') && empty(get_sub_field('external_link')))) : ?>
                    <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php elseif (!empty(get_sub_field('cta') && !empty(get_sub_field('external_link')))) :  ?>
                    <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php endif; ?>

            <div class="slider-nav-wrapper">
                <div class="slider-nav-prev-<?php echo $id; ?>">
                    <p class="heading-2">Prev</p>
                </div>
                <div class="slider-nav-next-<?php echo $id; ?>">
                    <p class="heading-2">Next</p>
                </div>
            </div>
        </div>
    </div>
</div>

