<div class="grid-container content-split content-split-left" data-module-key="<?php echo $moduleIndex['content_split_left'] ?>" id="content-split-left-<?php echo $moduleIndex['content_split_left']; ?>">
    <div class="grid-x grid-margin-y grid-margin-x grid-padding-x anim">
        <div class="cell large-6 xlarge-5 content">
            <div class="grid-x">
                <div class="cell small-12 large-10 large-offset-1 copy">
                    <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
                    <?php the_sub_field('description'); ?>
                    <!-- Check CTA/URL Type -->
                    <?php if (!empty(get_sub_field('cta') && empty(get_sub_field('external_link')))) : ?>
                        <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                    <?php elseif (!empty(get_sub_field('cta') && !empty(get_sub_field('external_link')))) :  ?>
                        <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="cell large-6 xlarge-7 image-block <?php echo (get_sub_field('divider') ? 'with-divider' : ''); ?>">
            <div class="image" style="background-image: url(<?php the_sub_field('image_block'); ?>);"></div>
        </div>
    </div>
</div>