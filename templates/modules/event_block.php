<div class="grid-container event-block" data-module-key="<?php echo $moduleIndex['event_block'] ?>" id="event-block-<?php echo $moduleIndex['event_block']; ?>">
    <img class="shadow-border shadow-offset" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
    <div class="grid-x grid-margin-x grid-padding-x anim">
        <div class="cell small-12 medium-6 medium-offset-0 content">
            <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>

            <?php if ( have_rows('dates') ) : ?>
                <?php while( have_rows('dates') ) : the_row(); ?>
                    <p><?php the_sub_field('date'); ?></p>
                <?php endwhile; ?>    
            <?php endif; ?>
            
            <div class="cta">
                <a href="<?php the_sub_field('cta_link'); ?>">
                    <img src="<?php the_sub_field('cta_icon'); ?>" alt="CTA Icon">
                    <p class="heading-2 uppercase"><?php the_sub_field('cta'); ?></p>
                </a>
            </div>
        </div>
        <div class="cell small-10 small-offset-1 medium-6 medium-offset-0 content-block">
            <div class="content-block-wrapper <?php (get_sub_field('hide_message_box') ? 'hide' : '') ?>">
                <p><?php the_sub_field('content_block'); ?></p>
            </div>
        </div>
    </div>
</div>