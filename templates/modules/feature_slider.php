<div class="feature-slider grid-container full" data-module-key="<?php echo $moduleIndex['feature_slider'] ?>" id="feature-slider-<?php echo $moduleIndex['feature_slider']; ?>">
    <div class="feature-slider-container">
        <div class="swiper-wrapper">
            <?php $slideIndex = 0; ?>
            <?php if ( have_rows('slides') ) : ?>
                <?php while( have_rows('slides') ) : the_row(); $slideIndex++; ?>
                    <?php if (get_sub_field('type') == 'image'): ?>
                        <div class="swiper-slide">
                            <div class="grid-x feature-image-container">
                                <div class="cell large-6">
                                    <img class="feature-image" src="<?php the_sub_field('image'); ?>" alt="feature image">
                                </div>
                                <div class="cell large-6 feature-content">
                                    <div class="content-wrapper">
                                        <span class="heading-1"><?php the_sub_field('heading'); ?></span>
                                        <p><?php the_sub_field('description'); ?></p>
                                         <!-- Check CTA/URL Type -->
                                        <?php if (!empty(get_sub_field('cta') && empty(get_sub_field('external_link')))) : ?>
                                            <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                                        <?php elseif (!empty(get_sub_field('cta') && !empty(get_sub_field('external_link')))) :  ?>
                                            <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    <?php endif; ?>

                    <?php if (get_sub_field('type') == 'video'): ?>
                        <div class="swiper-slide">

                        <?php if (!empty(get_sub_field('video_link'))) : ?>
                        <a href="<?php the_sub_field('video_link'); ?>" data-fancybox>
                        <?php endif; ?>
                            <div class="grid-x feature-image-container">
                                <div class="cell feature-video" style="background-image: url(<?php the_sub_field('video_image'); ?>);">
                                    <?php if (get_sub_field('video_heading') != '') : ?>
                                        <span class="heading-1"><?php the_sub_field('video_heading'); ?></span>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('video_description') != '') : ?>
                                        <p><?php the_sub_field('video_description'); ?></p>
                                    <?php endif; ?>
                                    <?php if (!empty(get_sub_field('video_link'))) : ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-button.svg" alt="Play Button">
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php if (!empty(get_sub_field('video_link'))) : ?>
                            </a>
                        <?php endif; ?>
                        </div>   
                    <?php endif; ?>
            
                <?php endwhile; ?>
            <?php endif; ?>
            
        </div>
    </div>
    <?php if ($slideIndex > 1) : ?>    
        <div class="feature-slider-controls"></div>
    <?php endif; ?>
</div>