<div class="gallery grid-container full grey" id="<?php the_sub_field('id_title'); ?>" data-module-key="<?php echo $moduleIndex['masonry_gallery'] ?>" id="gallery-<?php echo $moduleIndex['masonry_gallery']; ?>">
    <span class="heading-1"><?php the_sub_field('title'); ?></span>
    <div class="gallery-wrapper">
        <?php if ( have_rows('gallery_block') ) : ?>
            <?php while( have_rows('gallery_block') ) : the_row(); ?>
        
                <div class="gallery-block">
                    <div class="image <?php the_sub_field('size'); ?>" style="background-image: url(<?php the_sub_field('image'); ?>)"></div>
                    <p class="heading-1"><?php the_sub_field('title'); ?></p>
                    <a href="<?php the_sub_field('image'); ?>"><button class="btn-underline">Download</button></a>
                </div>
        
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>