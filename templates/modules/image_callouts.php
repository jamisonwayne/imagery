<div class="grid-container image-callouts" data-module-key="<?php echo $moduleIndex['image_callouts'] ?>" id="image-callouts-<?php echo $moduleIndex['image_callouts']; ?>">
    <div class="grid-x grid-margin-y anim">
        <div class="cell medium-6 medium-offset-3 large-10 large-offset-1">
            <div class="grid-x grid-margin-x grid-padding-x grid-padding-y">
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_1'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_1'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_1'); ?></p>
                        <!-- Check CTA/URL Type -->
                        <?php if (empty(get_sub_field('external_link_1'))) : ?>
                            <button class="btn-underline"><?php the_sub_field('cta_1'); ?></button>
                        <?php elseif (!empty(get_sub_field('external_link_1'))) :  ?>
                            <button class="btn-underline"><?php the_sub_field('cta_1'); ?></button>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_2'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_2'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_2'); ?></p>
                        <!-- Check CTA/URL Type -->
                        <?php if (empty(get_sub_field('external_link_2'))) : ?>
                            <button class="btn-underline"><?php the_sub_field('cta_2'); ?></button>
                        <?php elseif (!empty(get_sub_field('external_link_2'))) :  ?>
                           <button class="btn-underline"><?php the_sub_field('cta_2'); ?></button>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="cell large-4 callout-block">
                    <a href="<?php the_sub_field('cta_link_3'); ?>">
                        <div class="image" style="background-image: url(<?php the_sub_field('image_3'); ?>);"></div>
                        <p class="heading-1"><?php the_sub_field('title_3'); ?></p>
                        <!-- Check CTA/URL Type -->
                        <?php if (empty(get_sub_field('external_link_3'))) : ?>
                            <button class="btn-underline"><?php the_sub_field('cta_3'); ?></button>
                        <?php elseif (!empty(get_sub_field('external_link_3'))) :  ?>
                            <button class="btn-underline"><?php the_sub_field('cta_3'); ?></button>
                        <?php endif; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>