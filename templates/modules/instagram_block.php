<div class="grid-container instagram-block" data-module-key="<?php echo $moduleIndex['instagram_block'] ?>" id="instagram-block-<?php echo $moduleIndex['instagram_block']; ?>">
    <div class="grid-x anim">
        <div class="small-10 small-offset-1 medium-8 medium-offset-2 block">
            <h1 class="heading-2"><?php the_sub_field('main_heading'); ?></h1>
            <?php echo do_shortcode(get_sub_field('shortcode')); ?>
        </div>
    </div>
</div>