<?php 
    $backgroundClass;
    if (get_sub_field('background_color') == 'green'):
        $backgroundClass =  'green';
    elseif (get_sub_field('background_color') == 'purple'): 
        $backgroundClass = 'purple';
    endif;
?>

<div class="grid-container shop-callout" data-module-key="<?php echo $moduleIndex['shop_callout'] ?>" id="shop-callout-<?php echo $moduleIndex['shop_callout']; ?>">
    <div class="grid-x shop-callout-container anim">
        <div class="cell medium-7 large-8 image" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
        <div class="cell medium-5 large-4 content <?php echo $backgroundClass; ?>">
            <div class="content-wrapper">
                <span class="heading-1"><?php the_sub_field('main_heading'); ?></span>
               
                <!-- Check CTA/URL Type -->
                <?php if (!empty(get_sub_field('cta') && empty(get_sub_field('external_link')))) : ?>
                    <a href="<?php the_sub_field('cta_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php elseif (!empty(get_sub_field('cta') && !empty(get_sub_field('external_link')))) :  ?>
                    <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('cta'); ?></button></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>