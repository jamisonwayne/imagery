<div class="grid-container split-image-detail <?php echo (get_sub_field('align_text_right')) ? 'detail-right' : ''; ?> <?php echo (get_sub_field('align_text_middle')) ? 'detail-middle' : ''; ?>" data-module-key="<?php echo $moduleIndex['split_image_&_detail'] ?>" id="split-image-detail-<?php echo $moduleIndex['split_image_&_detail']; ?>">
    <div class="grid-x grid-margin-x anim">
        <div class="cell large-7 image <?php echo (get_sub_field('reverse')) ? 'small-order-2 large-order-2' : ''; ?> <?php echo (get_sub_field('add_links')) ? '' : 'medium-order-1'; ?>">
            <div class="image-block" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
            
            <?php if (get_sub_field('add_links')): ?>
                <div class="links">
                    <?php if ( have_rows('links') ) : ?>
                        <?php while( have_rows('links') ) : the_row(); ?>
                            <div class="link">
                                <!-- Check CTA/URL Type -->
                                <?php if (empty(get_sub_field('external_link'))) : ?>
                                    <a href="<?php the_sub_field('link'); ?>"><button class="btn-underline"><?php the_sub_field('link_title'); ?></button></a>
                                <?php elseif (!empty(get_sub_field('external_link'))) :  ?>
                                    <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-underline"><?php the_sub_field('link_title'); ?></button></a>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="cell large-5 detail <?php echo (get_sub_field('reverse')) ? 'small-order-1 large-order-1' : ''; ?> <?php echo (!get_sub_field('add_links')) ? 'small-order-2' : ''; ?>">
            <?php the_sub_field('detail'); ?>
        
            <!-- Check CTA/URL Type -->
            <?php if (!empty(get_sub_field('detail_cta') && empty(get_sub_field('external_link')))) : ?>
                <a href="<?php the_sub_field('detail_cta_link'); ?>"><button class="btn-black"><?php the_sub_field('detail_cta'); ?></button></a>
            <?php elseif (!empty(get_sub_field('detail_cta') && !empty(get_sub_field('external_link')))) :  ?>
                <a href="<?php the_sub_field('external_link'); ?>"><button class="btn-black"><?php the_sub_field('detail_cta'); ?></button></a>
            <?php endif; ?>
        </div>
        <!-- paragraph block -->
        <?php if (get_sub_field('paragraph_block')): ?>
            <div class="cell large-10 large-offset-1 small-order-3 paragraph-block">
                <?php the_sub_field('paragraph_block'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>