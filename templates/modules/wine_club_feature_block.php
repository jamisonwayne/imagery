<div class="wine-club-feature-block club-intro green">
    <div class="grid-container">
        <div class="grid-x grid-padding-y">
            <div class="cell medium-8 large-6 xlarge-5 xxlarge-4 content">
                <h1 class="heading-1"><?php the_sub_field('main_heading'); ?></h1>
                <p><?php the_sub_field('description'); ?></p>
            </div>
        </div>
    </div>
</div>

<div class="grid-container full wine-club-feature-block">
    <div class="grid-x grid-padding-y">
        <div class="cell disclaimer">
            <h2 class="paragraph-2"><?php the_sub_field('subheading'); ?></h2>
        </div>
        <div class="cell wine-club-bottles">
            <?php if ( have_rows('club_wines') ) : $index = 0; ?>                
                <?php while( have_rows('club_wines') ) : the_row(); $index++ ?>
                    <!-- Club Bottles -->
                    <div id="club-wine-<?php echo $index; ?>" class="club-wine club-wine-<?php $index; ?>" data-wine-id="<?php echo $index; ?>">
                        <img src="<?php the_sub_field('bottle'); ?>" alt="Wine Bottle">
                        <div class="wine-link">
                            <button class="btn-underline">View Details</button>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="grid-container full wine-club-feature-block">
    <div class="grid-x">
        <div id="wine-club-bottle-details" class="cell wine-club-bottle-details">
            <?php if ( have_rows('club_wines') ) : $index = 0; ?>                
                <?php while( have_rows('club_wines') ) : the_row(); $index++ ?>
                    <!-- Club Detail -->
                    <div id="club-detail-<?php echo $index; ?>" class="club-detail club-detail-<?php echo $index; ?> hide" data-wine-id="<?php echo $index; ?>">
                        <img class="shadow-border" src="<?php echo get_template_directory_uri(); ?>/assets/images/shadow-border-top.png" alt="Shadow Border Top">
                        <img class="close" src="<?php echo get_template_directory_uri(); ?>/assets/images/close.svg" alt="close icon">
                        <?php the_sub_field('description'); ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="cell disclaimer">
            <p class="italic"><?php the_sub_field('disclaimer_text'); ?></p>
        </div>
    </div>
</div>