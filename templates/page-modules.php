<?php 
/** Template Name: Modules */ 

get_header();
?>

<div class="modules">

    <?php 
        $moduleIndex = array(
            'feature_slider' => -1,
            'content_3_image_blocks' => -1,
            'content_&_image_callout' => -1,
            'content_split_left' => -1,
            'event_block' => -1,
            'content_scroll_slider' => -1,
            'content_3_image_callout' => -1,
            'content_block' => -1,
            'wine_club_feature_block' => -1,
            'split_image_&_detail' => -1,
            'image_callouts' => -1,
            'content_&_links' => -1,
            'masonry_gallery' => -1,
            'content_split_right' => -1,
            'shop_callout' => -1,
            'instagram_block' => -1,
        );
    ?>

    <?php if( have_rows('modules') ): ?>
        <?php while ( have_rows('modules') ) : the_row(); ?>

            <?php if( get_row_layout() == 'feature_slider' ): 
                $moduleIndex['feature_slider']++; 
                include 'modules/feature_slider.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_3_image_blocks' ):
                $moduleIndex['content_3_image_blocks']++; 
                include 'modules/content_3_image_blocks.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_&_image_callout' ):
                $moduleIndex['content_&_image_callout']++;  
                include 'modules/content_&_image_callout.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_split_left' ):
                $moduleIndex['content_split_left']++;  
                include 'modules/content_split_left.php';
            endif; ?>

            <?php if( get_row_layout() == 'event_block' ): 
                $moduleIndex['event_block']++; 
                include 'modules/event_block.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_scroll_slider' ):
                $moduleIndex['content_scroll_slider']++;  
                include 'modules/content_scroll_slider.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_3_image_callout' ):
                $moduleIndex['content_3_image_callout']++;  
                include 'modules/content_3_image_callout.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_block' ):
                $moduleIndex['content_block']++;  
                include 'modules/content_block.php';
            endif; ?>

            <?php if( get_row_layout() == 'wine_club_feature_block' ):
                $moduleIndex['wine_club_feature_block']++;  
                include 'modules/wine_club_feature_block.php';
            endif; ?>

            <?php if( get_row_layout() == 'split_image_&_detail' ):
                $moduleIndex['split_image_&_detail']++;  
                include 'modules/split_image_&_detail.php';
            endif; ?>

            <?php if( get_row_layout() == 'image_callouts' ):
                $moduleIndex['image_callouts']++;  
                include 'modules/image_callouts.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_&_links' ):
                $moduleIndex['content_&_links']++;  
                include 'modules/content_&_links.php';
            endif; ?>

            <?php if( get_row_layout() == 'masonry_gallery' ):
                $moduleIndex['masonry_gallery']++;  
                include 'modules/gallery.php';
            endif; ?>

            <?php if( get_row_layout() == 'content_split_right' ):
                $moduleIndex['content_split_right']++; 
                include 'modules/content_split_right.php';
            endif; ?>

            <?php if( get_row_layout() == 'shop_callout' ):
                $moduleIndex['shop_callout']++; 
                include 'modules/shop_callout.php';
            endif; ?>

            <?php if( get_row_layout() == 'instagram_block' ):
                $moduleIndex['instagram_block']++; 
                include 'modules/instagram_block.php';
            endif; ?>

        <?php endwhile; ?>
    <?php endif; ?>
    
</div>


<?php get_footer(); ?>

