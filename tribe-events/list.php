<?php
/**
 * List View Template
 * The wrapper template for a list of events. This includes the Past Events and Upcoming Events views
 * as well as those same views filtered to a specific category.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

do_action( 'tribe_events_before_template' );
?>

<!-- Title Bar -->
<?php //tribe_get_template_part( 'list/title-bar' ); ?>


<div class="grid-container imagery-calendar">
	<div class="cell">
		<div class="grid-x grid-margin-x grid-padding-x description">
			<h1 class="cell heading-1">Events Calendar</h1>
			<p class="cell large-6 paragraph-1">Join us for special events that celebrate the release of new varietals, holidays, memorable dinners, or Wine Club exclusive happenings. </p>
		</div>
		<div class="grid-x imagery-calendar-wrapper">
			<!-- Tribe Bar -->
			<?php //tribe_get_template_part( 'modules/bar' ); ?>
			<div class="cell medium-8 list-events">
				<!-- Main Events Content -->
				<div class="grid-x">
					<?php tribe_get_template_part( 'list/content' ); ?>
				</div>
			</div>


			<!-- Sidebar -->
			<div class="cell medium-4 list-sidebar">
				<div class="grid-x">
					<div class="cell small-10 small-offset-1">
						<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('events-sidebar') ) : ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="tribe-clear"></div>

<?php
do_action( 'tribe_events_after_template' );
