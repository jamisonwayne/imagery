<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>
<div id="tribe-events-content" class="tribe-events-single grid-container">
	<div class="grid-x grid-padding-x">

		<div class="cell small-10 small-offset-1 large-8 large-offset-2">
			
			<div class="grid-x">
				<!-- Select Categories -->
				<!-- <form action="http://localhost/imagery" method="get" class="cell small-8 small-offset-2 large-6 large-offset-3">
					<select name="tribe_events_cat" id="custom-post-type-categories-dropdown-single" class="postform">
						<option value="-1">Select Category</option>

						<?php
						$categories = get_categories(array(
							'taxonomy' => 'tribe_events_cat',
						));

						foreach ($categories as $category) {
							$category_link = sprintf( 
								'<option class="level-0" value="%2$s">%1$s</option>',
								esc_html( $category->name ),
								esc_html( $category->slug )
							);
							echo '<p class="category-link">' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</p> ';
						}
						?>
					</select>

					<script>
						(function() {
							/* <![CDATA[ */
							var dropdown = document.getElementById( "custom-post-type-categories-dropdown-single" );
							function onCatChange() {
								if ( dropdown.options[dropdown.selectedIndex].value ) {
									return dropdown.form.submit();
								}
							}
							dropdown.onchange = onCatChange;
						})();
							/* ]]&gt; */
					</script>

				</form> -->
			</div>
			<!-- Notices -->
			<div class="cell"><?php tribe_the_notices() ?></div>

			<!-- Title Info -->
			<div class="cell title-info">
				<a class="all-events-link" href="<?php echo bloginfo('url'); ?>/events">Back To All Events</a>
				<h1 class="heading-1"><?php the_title(); ?></h1>
				<div class="event_title_info">	
					<div class="tribe-events-schedule tribe-clearfix">
						<?php echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
					</div>
				</div>
			</div>

			<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'page-image', false ); ?>
	
			<div class="clear"></div>
			<?php while ( have_posts() ) :  the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<!-- Event content -->
					<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
					<div class="tribe-events-single-event-description tribe-events-content">
						<?php the_content(); ?>
					</div>
					<!-- .tribe-events-single-event-description -->
					<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

					<!-- Event meta -->
					<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
					<?php tribe_get_template_part( 'modules/meta' ); ?>
					<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
				</div> <!-- #post-x -->
				<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
			<?php endwhile; ?>

			<!-- Event footer -->
			<div id="tribe-events-footer">
				<!-- Navigation -->
				<nav class="tribe-events-nav-pagination" aria-label="<?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?>">
					<ul class="tribe-events-sub-nav">
						<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link('<button class="btn-underline">Previous</button>') ?></li>
						<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '<button class="btn-underline">Next</button>' ) ?></li>
					</ul>
					<!-- .tribe-events-sub-nav -->
				</nav>
			</div>
			<!-- #tribe-events-footer -->

		</div> <!-- end of Grid -->
		
	</div>
</div><!-- #tribe-events-content -->
<hr />